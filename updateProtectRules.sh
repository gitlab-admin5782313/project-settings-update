if [ -z "$1" ]; then
    read -p "Please enter your private token: " token
else
    token=$1
fi

if [ -z "$2" ]; then
    read -p "Please enter your group ID: " group_id
else
    group_id=$2
fi

gitlab_projects_ids=gitlab_projects_ids.csv
gitlab_base_uri="https://gitlab.com/api/v4"

# Build the list of project IDs
group_projects_uri="$gitlab_base_uri/groups/$group_id/projects?private_token=$token"
number_of_pages=$(curl -s --head "$group_projects_uri" | grep -i x-total-pages | awk '{print $2}' | tr -d '\r\n')

> $gitlab_projects_ids

#Build CSV file of projects
for page in $(seq 1 $number_of_pages); do
    curl -s "$group_projects_uri&page=$page" | jq -jr '.[] | .id,",",.name,"\n"' >> $gitlab_projects_ids
done

total_count=$(wc -l <$gitlab_projects_ids | tr -d ' ')
echo "Found $total_count projects in group ID $group_id."
read -n 1 -s -r -p "Press any key to continue."
echo 

updateBranchMergeRules()
{
    echo "\tEnsuring branch protect rule is in place for $1."
    
    #id=$(curl -s -H "Authorization: Bearer $token" -X GET $project_base_uri/protected_branches/main | jq -r ".merge_access_levels[0].id")
    #I could not get the update protect branch inplace API to work; I am working with support. For now, deleting and recreating.

    protected_branches=($(curl -s -H "Authorization: Bearer $token" -X GET $2/protected_branches | jq -r ".[].name"))
    if [[ "${protected_branches[*]}" =~ "$1" ]]; then
        curl -s -o /dev/null --show-error --fail --request DELETE --header "PRIVATE-TOKEN: $token" "$2/protected_branches/$1"
    fi

    curl -s -o /dev/null --show-error --fail --request POST \
            --header "PRIVATE-TOKEN: $token" \
            --header "Content-Type: application/json" \
            --data '{"name": "'"$1"'", "allowed_to_push": [{"access_level": 0}], "allowed_to_merge": [{"access_level": 30}]}' \
            "$2/protected_branches"
}

count=1
while IFS=, read id name; do
    echo "Project $count/$total_count: $id - $name"

    project_base_uri="$gitlab_base_uri/projects/$id"
    echo "\tPrevent pushing secret files - setting to true."
    curl -s -o /dev/null --show-error --fail -H "Authorization: Bearer $token" -X PUT $project_base_uri/push_rule?prevent_secrets=true
    
    echo "\tRemove all approvals on push - setting to true."
    curl -s -o /dev/null --show-error --fail -H "Authorization: Bearer $token" -X POST $project_base_uri/approvals?reset_approvals_on_push=true
    
    echo "\tRequired number of approvals - setting to 2."
    curl -s -o /dev/null --show-error --fail -H "Authorization: Bearer $token" -X POST $project_base_uri/approvals?approvals_before_merge=2
    
    # Build list of branches
    branches=($(curl -s -H "Authorization: Bearer $token" -X GET $project_base_uri/repository/branches | jq -r ".[].name"))
    echo "\tProject contains the following branches: ${branches[@]}"

    if [[ "${branches[*]}" =~ "develop" ]]; then
        updateBranchMergeRules "develop" $project_base_uri
    fi

    if [[ "${branches[*]}" =~ "main" ]]; then
        updateBranchMergeRules "main" $project_base_uri
    elif [[ "${branches[*]}" =~ "master" ]]; then
        updateBranchMergeRules "master" $project_base_uri
    fi

    count=$((count+1))
    echo
done < $gitlab_projects_ids
